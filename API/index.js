const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const fs = require('fs');


app.use(bodyParser.json());


app.post('/register', (req, res) => {
    let username = req.body.username;
    let password = req.body.password;
    let mail = req.body.mail;
    let age = req.body.age;

    let tempUserInformation = {
        username : username,
        password : password,
        mail : mail,
        age : age
    }

    fs.readFile('database.json', (err, data) => {
        if(err) throw err;
        let database = JSON.parse(data);

        let registeredmail = database.filter( (item) => {
            return item.mail == mail;
        }) [0];

        if(registeredmail) {
            res.json({
                status : 409,
                message : 'exist'
            })
        } else {
            database[0].push(tempUserInformation);

            fs.writeFile('database.json', JSON.stringify(database), (err) => {
                if(err) throw err;
            })
    
            res.json({
                'status' : 200,
                'message' : 'Registred'
            })
        }  
    })
})

app.post('/login', (req,res) => {
    let username = req.body.username;
    let password = req.body.password;

    fs.readFile('database.json', (err, data) => {
        if(err) throw err;

        let database = JSON.parse(data);
        let registereduser = database[0].filter( (item) => {
            return item.username == username && item.password == password;
        }) [0]

        if(!registereduser) {
            res.json({
                status : 404,
                message : 'Student not found'
            })
        } else {
            res.json({
                status : 200,
                message : 'Student found'
            })
        }
    })
})

app.get('/student', (req,res) => {
   

    fs.readFile('database.json', (err, data) => {
        if(err) throw err;
        let studentsList = JSON.parse(data);
        res.json(studentsList);
        // res.json(studentsList[2]);
    })
})

app.get('/student', (req,res) => {
    fs.readFile('database.json', (err, data) => {
        if(err) throw err;
        let statisticsList = JSON.parse(data);
        res.json(statisticsList);
        
    })
})

app.post('/student/addstudent', (req, res) => {
    let id = req.body.stdid;
    let name = req.body.stdname;
    let surname = req.body.stdsurname;
    let grade = req.body.stdgrade;
    

    let tempStudentInformation = {
        id : id,
        name : name,
        surname : surname,
        grade : grade
    }

    fs.readFile('database.json', (err, data) => {
        if(err) throw err;
        let database = JSON.parse(data);

        let students = database[1].filter( (item) => {
            return item.name == name;
        }) [0];

        if(students) {
            res.json({
                status : 409,
                message : 'exist'
            })
        }
         else {
            database[1].push(tempStudentInformation);

            fs.writeFile('database.json', JSON.stringify(database), (err) => {
                if(err) throw err;
            })
    
            res.json({
                'status' : 200,
                'message' : 'Registred'
            })
        }
    })
})

app.post('/student/adddate', (req, res) => {
    let date = req.body.date;
    let grade = req.body.grade;

    let tempDateInformation = {
        date : date
        // grade : grade
    }

    let tempGradeInformation = {
        // date : date,
        grade : grade
    }
    

    fs.readFile('database.json', (err, data) => {
        if(err) throw err;

        let database = JSON.parse(data);
        let dates = database[2];
        let grades = database[1]
        

        if(!dates && !grades) {
            res.json({
                status : 404,
                message : 'not found'
            })
        }
         else {
            dates.push(tempDateInformation['date']);
            grades.push(tempGradeInformation);
            
           
            fs.writeFile('database.json', JSON.stringify(database), (err) => {
                if(err) throw err;
            })
    
            res.json({
                'status' : 200,
                'message' : 'added'
            })
        }
    })
})
app.listen(5000);