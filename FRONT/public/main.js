var regBtn = document.getElementById('regBtn');
var logBtn = document.getElementById('logBtn');
var access = document.getElementById('access');
var regField = document.getElementById('regfield');
var loginField = document.getElementById('loginfield');


regBtn.addEventListener('click', () => {
    if(regField.style.display == "none" && loginField.style.display == "none" && successful.style.display == "block" || regField.style.display == "none" && loginField.style.display == "block" || regField.style.display == "none" && loginField.style.display == "none" && successful.style.display == "none" || regField.style.display == "block" && loginField.style.display == "none" && successful.style.display == "none") {
        regField.style.display = "block";
        loginField.style.display = "none";
        successful.style.display = "none";
        checkname.innerText = "";
        checkpass.innerText = "";
        checkpassconf.innerText = "";
        checkmail.innerText = "";
        checkage.innerText = "";
    }
})

logBtn.addEventListener('click', () => {
    if(loginField.style.display == "none" && regField.style.display == "none" && successful.style.display == "block" || regField.style.display == "block" && loginField.style.display == "none" || loginField.style.display == "none" && regField.style.display == "none" && successful.style.display == "none" || regField.style.display == "none" && loginField.style.display == "block" && successful.style.display == "none") {
        loginField.style.display = "block";
        regField.style.display = "none";
        successful.style.display = "none"
        checklogin.innerText = "";
    }
})

var regUsenameValue = document.getElementById('regusername');
var regPasswordValue = document.getElementById('regpassword');
var regPasswordConf = document.getElementById('regpasswordconf');
var regMailValue = document.getElementById('regemail');
var regAgeValu = document.getElementById('regage');
var regSubmitBtn = document.getElementById('regsubmitBtn');

var successful = document.getElementById('successful');

var checkname = document.getElementById('checkname');
var checkmail = document.getElementById('checkmail');
var checkpass = document.getElementById('checkpass');
var checkpassconf = document.getElementById('checkpassconf');
var checkage = document.getElementById('checkage');


regUsenameValue.addEventListener('focus', () => {
    checkname.innerText = "";
})
regPasswordValue.addEventListener('focus', () => {
    checkpass.innerText = "";
})
regPasswordConf.addEventListener('focus', () => {
    checkpassconf.innerText = "";
})
regMailValue.addEventListener('focus', () => {
    checkmail.innerText = "";
})
regAgeValu.addEventListener('focus', () => {
    checkage.innerText = "";
})


regSubmitBtn.addEventListener('click', () => {

    if(regUsenameValue.value == "" && regPasswordValue.value == "" && regPasswordConf.value == "" && regMailValue.value == "" && regAgeValu.value == "") {
        checkname.innerText = "* Please enter your Name in the form";
        checkpass.innerText = "* Please enter your Password in the form";
        checkpassconf.innerText = "* Please confirm your Password in the form";
        checkmail.innerText = "* Please enter your Email in the form";
        checkage.innerText = "* Please enter your Age in the form";
    } else if(regUsenameValue.value == "") {
        checkname.innerText = "* Please enter your Name in the form";
    } else if(regPasswordValue.value == "") {
        checkpass.innerText = "* Please enter your Password in the form";
    } else if(regPasswordConf.value == "") {
        checkpassconf.innerText = "* Please confirm your Password in the form";
    } else if(regMailValue.value == "") {
        checkmail.innerText = "* Please enter your Email in the form";
    } else if(regAgeValu.value == "") {
        checkage.innerText = "* Please enter your Age in the form";
    } else  {
        if(regPasswordValue.value === regPasswordConf.value) {
            let regUserData = {
                username : regUsenameValue.value,
                password : regPasswordValue.value,
                mail : regMailValue.value,
                age : regAgeValu.value
            }
        
        
            var xhr = new XMLHttpRequest();
            xhr.open('POST', "http://localhost:5000/register");
            xhr.setRequestHeader('Content-Type', 'application/json');
            xhr.send(JSON.stringify(regUserData));
            xhr.onload = function(regUserData) {
                
                var res = JSON.parse(this.response);
                if(res.status == 200 && successful.style.display == "none") {
                    successful.style.display = "block";
                    regField.style.display = "none";
                    successful.innerText = "You Registered Succssfully";
                    regUsenameValue.value = "";
                    regPasswordValue.value = "";
                    regPasswordConf.value = "";
                    regMailValue.value = "";
                    regAgeValu.value = "";
                } else if(res.status == 409 && successful.style.display == "none") {
                    checkmail.innerText  = "Email you entered already exist! Please try another";
                } else {
                    successful.innerText = "Please try again";
                }
            }
        } else {
            checkpassconf.innerText  = "Your Password does not match";
        }
      }
})

var loginSubmitBtn = document.getElementById('loginsubmitBtn');
var loginUsername = document.getElementById('loginusername');
var loginPassword = document.getElementById('loginpassword');
var checklogin = document.getElementById('checklogin');

loginUsername.addEventListener('focus', () => {
    checklogin.innerText = "";
})
loginPassword.addEventListener('focus', () => {
    checklogin.innerText = "";
})

loginSubmitBtn.addEventListener('click', () => {

    let loginPerson = {
        username : loginUsername.value,
        password : loginPassword.value
    }
    

    var xhttp = new XMLHttpRequest();
    xhttp.open('POST', "http://localhost:5000/login");
    xhttp.setRequestHeader('Content-Type', 'application/json');
    xhttp.send(JSON.stringify(loginPerson));
    xhttp.onload = function(loginPerson) {
        var res = JSON.parse(this.response);
        if(res.status == 200) {
            window.location.href = "http://localhost:5200/student";
        } else if(res.status == 404 && successful.style.display == "none") {
            checklogin.innerText  = "Username or Password is incorrect";
        }
    }
})
