const express = require('express');
const app = express();
const path = require('path');

// ss.mean(this.addGrade())


app.use(express.static(path.join(__dirname, 'public')));

app.get('/', (req, res) => {
    res.sendFile(path.join(__dirname + '/public/index.html'))
});


app.get('/student', (req, res) => {
    res.sendFile(path.join(__dirname + '/public/student.html'))
})

app.listen(5200);